package source.dialog;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 *
 * @author bkowalczyyk
 */
public class Dialogs implements DialogContract{

    private Stage dialog = new Stage();
    private VBox vb = new VBox();
    private int height, width;


    public Dialogs(){
        height = 170;
        width = 380; // default height and width set to 170x380
        dialog.setResizable(false);
    }
    public void defaultDialog(DialogType type, String inf){

        dialog.setTitle(type.getTitle());
        
        File file = new File(System.getProperty("user.dir")+"\\src\\image\\"+type.getType());
        Image img = new Image(file.toURI().toString());
        ImageView imgView = new ImageView(img);

        
        HBox hb = new HBox();
        hb.setAlignment(Pos.BOTTOM_RIGHT);
        Button button = new Button("proceed");
        button.setStyle("-fx-font: 12 arial;"
                + "-fx-background-radius: 15 15 15 15; -fx-background-insets: 1 1 1 1");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        hb.getChildren().add(button);
        if(inf.length()<=100){
            hb.setPadding(new Insets(45, 10, 10, 60));
        }else{
            hb.setPadding(new Insets(30, 10, 10, 60));
        }
        
    
        HBox hb2 = new HBox();
        hb2.setAlignment(Pos.BOTTOM_CENTER);
        Text text = new Text();
        if(inf.length()<=150){
            text.setText(inf);
        }else{
            text.setText(inf.substring(0, 150)+"...");//better when information 
            //have less than 150 characters.
        }
        text.setWrappingWidth(350);
        text.minHeight(Double.MAX_VALUE);
        text.maxHeight(Double.MAX_VALUE);
        hb2.setPadding(new Insets(20, 10, 10, 10));
        hb2.getChildren().add(text);
        
        //MAX 150
        
        vb.getChildren().add(imgView);
        vb.getChildren().add(hb2);
        vb.getChildren().add(hb);
        
        
   
        show();
    }
    
    @Override
    public void show(){
        Scene scene = new Scene(vb, width, height);
        dialog.setScene(scene);
        dialog.show();
    }
    

    
}
