package source.dialog;

/**
 *
 * @author bkowalczyyk
 */

public enum DialogType{
    ERROR("error.png","Error Dialog"),
    INFO("info.png","Information Dialog"),
    ALERT("alert.png","Alert Dialog");
    
    private String type;
    private String title;
    DialogType(String type, String title){
        this.type = type;
        this.title = title;
    }
    public String getType(){
        return type;
    }
    public String getTitle(){
        return title;
    }
}