package source.dialog;

/**
 *
 * @author bkowalczyyk
 */
public interface DialogContract {
     public void show();
}
