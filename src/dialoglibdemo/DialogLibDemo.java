/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dialoglibdemo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import source.dialog.DialogType;
import source.dialog.Dialogs;

/**
 *
 * @author bkowalczyyk
 */
public class DialogLibDemo extends Application {
    
    private String information = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            + " Integer porttitor scelerisque auctor. Fusce id urna vel quam porta eleifend a vitae justo.";
    private String test;
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("OPEN DIALOG");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new Dialogs().defaultDialog(DialogType.ERROR,information);
                new Dialogs().defaultDialog(DialogType.ALERT,information);
                new Dialogs().defaultDialog(DialogType.INFO,information);
            }
        });
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
